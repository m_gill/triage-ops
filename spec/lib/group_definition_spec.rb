# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/group_definition'

RSpec.describe GroupDefinition do
  described_class::DATA.each_key do |name|
    describe "#group_#{name}" do
      it 'returns assignees and labels as arrays, optionally mentions too' do
        subject = described_class.public_send("group_#{name}")

        expect(subject).to include(
          assignees: a_kind_of(Array),
          labels: a_kind_of(Array)
        )

        expect(subject[:mentions]).to be_a_kind_of(Array).or(be_nil)
      end

      it 'returns assignees only from pm if specified' do
        subject = described_class.public_send("group_#{name}", ['pm'])

        assignees = described_class::DATA.dig(name, 'pm') || []

        expect(subject).to include(assignees: assignees)
      end

      it 'returns assignees from both pm and backend_engineering_manager when specified' do
        subject = described_class.public_send("group_#{name}", ['pm', 'backend_engineering_manager'])

        assignees =
          (
            (described_class::DATA.dig(name, 'pm') || []) +
            (described_class::DATA.dig(name, 'backend_engineering_manager') || [])
          ).uniq

        expect(subject).to include(assignees: assignees)
      end

      it 'returns mentions with extra mentions if specified' do
        subject = described_class.public_send(
          "group_#{name}", ['pm', 'extra_mentions'])

        assignees = described_class::DATA.dig(name, 'pm') || []
        extra_mentions = described_class::DATA.dig(name, 'extra_mentions')
        mentions = assignees + extra_mentions if extra_mentions

        if mentions
          expect(subject).to include(assignees: assignees, mentions: mentions)
        else
          expect(subject).to include(assignees: assignees)
        end
      end

      it 'returns defined labels or guessed group labels' do
        subject = described_class.public_send("group_#{name}")

        labels = described_class::DATA.dig(name, 'labels') ||
          ["group::#{name.tr('_', ' ')}"]

        expect(subject).to include(labels: labels)
      end
    end

    describe '#pm_for_team' do
      it 'returns pm for team' do
        expected_pm = described_class::DATA.dig(name, 'pm')&.first

        expect(described_class.pm_for_team(name)).to eq(expected_pm)
      end
    end

    describe '#em_for_team' do
      context 'backend' do
        it 'returns backend em for team' do
          expected_em = described_class::DATA.dig(name, 'backend_engineering_manager')&.first || described_class::DATA.dig(name, 'engineering_manager')&.first

          expect(described_class.em_for_team(name)).to eq(expected_em)
        end
      end

      context 'frontend' do
        let(:scope) { :frontend }

        it 'returns frontend em for team' do
          expected_em = described_class::DATA.dig(name, 'frontend_engineering_manager')&.first

          expect(described_class.em_for_team(name, scope)).to eq(expected_em)
        end
      end
    end
  end
end
