# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../group_definition'

module Generate
  module GroupPolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), nil, '-')

      FileUtils.rm_rf("#{destination}/#{template_name}", secure: true)
      FileUtils.mkdir_p("#{destination}/#{template_name}")

      GroupDefinition::DATA.each do |name, definition|
        next if definition['ignore_templates']&.include?(options.template)

        group_method_name = "group_#{name}"

        File.write(
          "#{destination}/#{template_name}/#{name}.yml",
          erb.result_with_hash(
            group_method_name: group_method_name,
            group_label_name:
              GroupDefinition.public_send(group_method_name).dig(:labels, 0)
          )
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
